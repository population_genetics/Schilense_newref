Scripts and data sets of the manuscript: 

**Contrasting patterns of presence-absence variation of NLRS within S. chilense are mainly shaped by past demographic history** 

**Authors:** Gustavo A. Silva-Arias, Edeline Gagnon, Surya Hembrom, Alexander Fastner, Muhammad Ramzan Khan, Remco Stam and Aurélien Tellier

https://doi.org/10.1101/2023.10.13.562278



0. DNA sequence of designed baits to capture Nb-Arc loci

- [NB-Arc_baits.fna](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/NB-Arc_baits.fna)

1. Raw read cleaning, quality check, _de novo_ assembly and extraction of Nb-Arc loci using Captus https://edgardomortiz.github.io/captus.docs/

- [run_captus_v0951.sh](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/run_captus_v0951.sh)

2. R notebook for filtering the extraction results of the Nb-Arc loci and plot the results

- [captus_filtering.Rmd](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/captus_filtering.Rmd)

3. Loci and samples/population data

- [Nb-Arc loci data](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/NLR_gene_list.txt)
- [Samples/population data](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/samples_Populations.txt)

4. Short read mapping to the reference and depth calculations

- [short-read_alignment.sh](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/read-mapping/short-read_alignment.sh)
- [depth_Nb-Arc.sh](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/dread-mapping/epth_Nb-Arc.sh)

5. Variant calling and summary statistics calculations

- [variant_calling.sh](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/SumStats/variant_calling.sh)
- [div_stats_calculations.sh](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/SumStats/div_stats_calculations.sh)
- [PAV Fst calculations](https://gitlab.lrz.de/population_genetics/Schilense_newref/-/blob/main/SumStats/PAV_Fst_calculations_v2.R)
