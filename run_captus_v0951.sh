conda activate captus

workdir=/data/proj2/popgen/chilense/capture
cd $workdir

####### running captus clean ########
captus_assembly clean -r $workdir/raw_fq --adaptor_set $workdir/scripts/custom_adapters.fa
####### captus clean -> DONE ########

####### running captus assemble ########
captus_assembly assemble -r $workdir/01_clean_reads \
--preset WGS --max_contig_gc 60
####### captus assemble -> DONE ########

####### running captus extract NLR ########
captus_assembly extract -a $workdir/02_assemblies \
-o $workdir/03_extractions/S_chilense_NLRs_new \
-n $workdir/scripts/ref_proteins/Dovetail_output.nbarc.fasta
####### captus extract -> DONE ########
