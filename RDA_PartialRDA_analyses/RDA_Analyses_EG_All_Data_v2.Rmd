---
title: "R Notebook"
output: html_notebook
---
#Written by Edeline Gagnon, May 2023

The purpose of this Notebook is to run a constrained ordination analysis on the NLR freq and PAV across different Solanum chilense populations. I will try two different types of analysis
(1) Redundancy analysis (https://r.qcbs.ca/workshop10/book-en/redundancy-analysis.html)
(2) Constrained Correspondance Analysis (from Chapter 6 of Numerical R with Ecology)

```{r}
#First we load the relevant libraries and set up the directory


## Install the required packages
#install.packages("vegan")
#installpackages("labdsv")
#install.packages("MASS")
#install.packages("ggplot2")

## install mvpart from package archive file
#install.packages("remotes")
#remotes::install_url("https://cran.r-project.org/src/contrib/Archive/mvpart/mvpart_1.6-2.tar.gz")

## Load the required packages
library(labdsv)
library(vegan)
library(MASS)
#library(mvpart)
library(ggplot2)
library(tidyverse)

setwd("C://Users/edeli/OneDrive/Projets_Recherche/2021_Tomato_herbarium_Rgenes/2022_DTGenome_TC/S_chilense_annotations/RDA_Analysis")

rm(list = ls())

ls()

```




Then we load the datasets
```{r}
# Reading the Table containing the PAV (value) and frequency of different NLRs, across all populations.
# This is in a long format, and I need to transform it into a wide format

spe <- read.csv("data/NLR_PAV_Freq_pop.csv", row.names = 1)
names(spe)

spe.wide<-spe %>% 
  #Select only the NBS and freq columns
  select(name,NBS,freq)%>% 
  spread(NBS, freq)

rownames(spe.wide)<-spe.wide$name
dim(spe.wide[,-1])
head(spe.wide[,-1])
spe.wide2<-spe.wide[,-1]

str(spe.wide2) # structure of objects in dataset
summary(spe.wide2) # summary statistics for all objects (min, mean, max, etc.)

# Count number of species frequencies in each abundance class
ab <- table(unlist(spe.wide2))
# Plot distribution of species frequencies
barplot(ab, las = 1, # make axis labels perpendicular to axis
        xlab = "Abundance class", ylab = "Frequency", # label axes
        col = grey(5:0/5)) # 5-colour gradient for the bars


```
```{r}
#Here, import a list of NLR attributes, identify the CNL group, and then subset the previous table columns names to only retain these  
#nlr.att <- read.csv("data/NLR_attributes.csv", row.names = 1)

#table(nlr.att$CnlTnlRnl)
#CNL RNL TNL 
#128  22  27 

#dim(nlr.att[nlr.att$CnlTnlRnl == 'CNL',])
#cnl<-nlr.att[nlr.att$CnlTnlRnl == 'CNL',]$NBS #This gives the list of NBS that are CNL loci.

#dim(spe.wide2 %>% select(matches(cnl)))#113

# spe.wide3<- spe.wide2 %>% select(matches(cnl))
#dim(spe.wide2[names(spe.wide2) %in% cnl]) #Verifying another way, it also works.

```


```{r}
# Count the number of zeros in the dataset
sum(spe.wide2 == 0) #62

# Calculate proportion of zeros in the dataset
sum(spe.wide2 == 0)/(nrow(spe.wide2)*ncol(spe.wide@))
#0.027

# Apply Hellinger transformation to correct for the double zero problem

spe.hel <- decostand(spe.wide3, method = "hellinger")


```


```{r}
## Loading now the environmental data
env <- read.csv("data/SChil_env.csv")

#First 5 columns are the environmental variables
#Other colummns are names and metadata associated with the different localities.

loc.names<-unique(spe.wide$name)
loc.names

#Here, I want to select only the sites that are present in the NLR abundance table.
#These are stored in the column TC
env2<-env %>% filter(TC=="yes")
dim(env)
dim(env2)


##There is an issue which is that the SHG and SCG actually represent several sites, and I will take the mean of the environmental variables for now.


##Select only the SHG and SCG in Label3, calculate mean, and then merge with the other 
env.SHG<-env2 %>% 
  filter(Label3=="SHG")%>%
  select(MODCF_intraannualSD, MODCF_monthlymean_09, CHELSA_bio10_06, CHELSA_bio10_07, CHELSA_bio10_13,elevation_1KMmd_SRTM,Label3) %>% 
  #Select only the environmental data
  summarise(across(everything(), mean)) #Calculate the means
env.SHG$Label2<-"Highland"
env.SHG$Label3<-"Highland"
env.SHG

env.SCG<-env2 %>% 
  filter(Label3=="SCG")%>%
  select(MODCF_intraannualSD, MODCF_monthlymean_09, CHELSA_bio10_06, CHELSA_bio10_07, CHELSA_bio10_13,elevation_1KMmd_SRTM,Label3) %>% 
  #Select only the environmental data
  summarise(across(everything(), mean)) #Calculate the means
env.SCG$Label2<-"Coast"
env.SCG$Label3<-"Coast"
env.SCG

#Select only the CG localities, and then merge with env.SCG and env.SHG 
env.CG<-env2 %>% 
  filter(Label2!="Coast") %>% 
  filter(Label2!="Highland") %>% 
  select(MODCF_intraannualSD, MODCF_monthlymean_09, CHELSA_bio10_06, CHELSA_bio10_07, CHELSA_bio10_13, elevation_1KMmd_SRTM, Label3, Label2)

dim(env.CG)

env.all<-rbind(env.CG,env.SHG,env.SCG)

names(env.all)
dim(env.all) #20 7
head(env.all)

str(env)
summary(env)
```


```{r}
# We can visually look for correlations between variables:
heatmap(abs(cor(env.all[,-c(6,7,8)])), 
        # Compute pearson correlation (note they are absolute values)
        col = rev(heat.colors(6)), 
        Colv = NA, Rowv = NA)
legend("topright", 
       title = "Absolute Pearson R",
       legend =  round(seq(0,1, length.out = 6),1),
       y.intersp = 0.7, bty = "n",
       fill = rev(heat.colors(6)))

# Scale and center variables
env.z <- decostand(env.all[,-c(6,7,8)], method = "standardize")

# Variables are now centered around a mean of 0
round(apply(env.z, 2, mean), 1)

# and scaled to have a standard deviation of 1
apply(env.z, 2, sd)
```



```{r}
# Model the effect of all environmental variables on fish community composition
spe.rda <- rda(spe.wide2 ~ ., data = env.z)

summary(spe.rda)

```



```{r}
######## RDA with raw data

spe.rda.signif<-rda(spe.wide2 ~ ., data = env.z)

# check the adjusted R2 (corrected for the number of explanatory variables)
RsquareAdj(spe.rda.signif)

anova.cca(spe.rda.signif, step = 1000)

anova.cca(spe.rda.signif, step = 1000, by = "term")

anova.cca(spe.rda.signif, step = 1000, by = "axis")

# Type 1 scaling
ordiplot(spe.rda.signif, scaling = 1, type = "text",choices=c(1,2))
# Type 2 scaling
ordiplot(spe.rda.signif, scaling = 2, type = "text")
```




```{r}

######## RDA with hellinger transformation of data

spe.rda.signif<-rda(spe.hel ~ ., data = env.z)
#spe.rda.signif<-rda(spe.hel ~ ., data = env.z)

# check the adjusted R2 (corrected for the number of explanatory variables)
RsquareAdj(spe.rda.signif)

print("######## Permutation test for rda, global")
anova.cca(spe.rda.signif, step = 1000)

print("######## Permutation test for rda, by term")

anova.cca(spe.rda.signif, step = 1000, by = "term")

print("######## Permutation test for rda, by axis")

anova.cca(spe.rda.signif, step = 1000, by = "axis")

# Type 1 scaling
ordiplot(spe.rda.signif, scaling = 1, type = "text")

# Type 2 scaling
ordiplot(spe.rda.signif, scaling = 2, type = "text")

```



```{r}
#Trying to do these plots more elegantly, with RDA.
#Following this website: https://rstudio-pubs-static.s3.amazonaws.com/694016_e2d53d65858d4a1985616fa3855d237f.html#4_Example_2:_Dune_meadow_data_with_constrained_ordination

##########
#Libraries
library(BiodiversityR) # also loads vegan
library(ggplot2)
library(readxl)
library(ggsci)
library(ggrepel)
library(ggforce)

################ 
# For scaling 2
# Longer arros maean this variable strongly drieves the 
plot2<-ordiplot(spe.rda.signif,choices=c(1,2)) #This is in scaling2


#This helps calculate the different RDA scores that will be useful for plotting.
sites.long2 <- sites.long(plot2, env.data=env.z)
head(sites.long2)
sites.long2$label2<-c("Southern Coastal Group",rep("Central Group",18), "Southern Highland Group")
sites.long2$Altitude<-c(346.3,1337,1412.5,2217,2735,1422.5,1748,2632,3073.5,3094.5,351,816,1102,1944.5,3292,1621,1903,2805,3483,2793)

species.long2 <- species.long(plot2)
species.long2

axis.long2 <- axis.long(spe.rda.signif, choices=c(1, 2))
axis.long2

sc_bp <- as.data.frame(vegan::scores(spe.rda.signif, display="bp", choices=c(1, 2), scaling=2))

```




```{r}
#Reducing the number of NLRs (or species in the website example), that explain most of the variation.

#Envfit: usually fits environmental vectors or factors onto an ordination, but here we use the NLR abundances as a dependent variable that is explained by the ordination scores. So we can see which NLRs have the greatest goodness of fit to the ordination axes.
spec.envfit <- envfit(plot2, env=spe.hel)
spec.data.envfit <- data.frame(r=spec.envfit$vectors$r, p=spec.envfit$vectors$pvals)
species.long2 <- species.long(plot2, spec.data=spec.data.envfit)
species.long2

#selecting NLRs that explain 60% of the variation
species.long3 <- species.long2[species.long2$r >= 0.6, ]
species.long3 

#4 NLRs

#116 and 153 have completely disapeared from Coastal pops. 32 and 140 are present across all 6 groups, but are much more frequent in some populations than others.
```


```{r}
plotgg2 <- ggplot() + 
    geom_vline(xintercept = c(0), color = "grey70", linetype = 2) +
    geom_hline(yintercept = c(0), color = "grey70", linetype = 2) +  
    xlab(axis.long2[1, "label"]) +
    ylab(axis.long2[2, "label"]) +  
    scale_x_continuous(sec.axis = dup_axis(labels=NULL, name=NULL)) +
    scale_y_continuous(sec.axis = dup_axis(labels=NULL, name=NULL)) +    
    geom_point(data=sites.long2, 
               aes(x=axis1, y=axis2, color=label2, shape=label2), 
               size=3) +
      geom_text_repel(data=sites.long2, 
                    aes(x=axis1, y=axis2, label=labels),
                    colour="black",size=3) +
        geom_point(data=species.long2, 
               aes(x=axis1, y=axis2),colour="red", 
               size=1) +
#    geom_segment(data=species.long3, 
 #                aes(x=0, y=0, xend=axis1, yend=axis2), 
#                 colour="red", size=0.2) +
#    geom_text_repel(data=species.long3, 
#                    aes(x=axis1, y=axis2, label=labels),
#                    colour="red",size=2) +
    geom_segment(data=sc_bp, 
                 aes(x=0, y=0, xend=RDA1, yend=RDA2), 
                 colour="blue", size=0.2,arrow=arrow())+ #+
    geom_text_repel(data=sc_bp, 
                    aes(x=RDA1, y=RDA2, label=rownames(sc_bp)),
                    colour="blue",size=4) +
  ggtitle("RDA ordination biplot, environmental variables and frequency of NLR loci",subtitle = "across 20 S.chilense populations (scaling 2)")+
  theme_minimal()+
#    BioR.theme +
#    ggsci::scale_colour_npg() +
  scale_fill_manual(values = c("Central Group"="#F8766D","Highland"="#00A5FF","Coast"="#00B81F"))+
    coord_fixed(ratio=1)

plotgg2
ggsave("RDA_ALL_sc2.pdf")
```



```{r}
#############
#Repeating this graph with scaling 1
# This means: that sites (populations)that are closer together are more similar; species (Nlrs) that are closer together have more sites in common.

plot1<-ordiplot(spe.rda.signif,choices=c(1,2),scaling=1)#THis is in scaling 1


#This helps calculate the different RDA scores that will be useful for plotting.
sites.long1 <- sites.long(plot1, env.data=env.z)
head(sites.long1)
sites.long1$label2<-c("Southern Coastal Group",rep("Central Group",18), "Southern Highland Group")
sites.long1$Altitude<-c(346.3,1337,1412.5,2217,2735,1422.5,1748,2632,3073.5,3094.5,351,816,1102,1944.5,3292,1621,1903,2805,3483,2793)

species.long1 <- species.long(plot1)
species.long1

axis.long1 <- axis.long(spe.rda.signif, choices=c(1, 2))
axis.long1

sc_bp1 <- as.data.frame(vegan::scores(spe.rda.signif, display="bp", choices=c(1, 2), scaling=1))


plotgg2 <- ggplot() + 
    geom_vline(xintercept = c(0), color = "grey70", linetype = 2) +
    geom_hline(yintercept = c(0), color = "grey70", linetype = 2) +  
    xlab(axis.long2[1, "label"]) +
    ylab(axis.long2[2, "label"]) +  
    scale_x_continuous(sec.axis = dup_axis(labels=NULL, name=NULL)) +
    scale_y_continuous(sec.axis = dup_axis(labels=NULL, name=NULL)) +    
    geom_point(data=sites.long1, 
               aes(x=axis1, y=axis2, color=label2, shape=label2), 
               size=3) +
      geom_text_repel(data=sites.long1, 
                    aes(x=axis1, y=axis2, label=labels),
                    colour="black",size=3) +
        geom_point(data=species.long1, 
               aes(x=axis1, y=axis2),colour="red", 
               size=1) +
#    geom_segment(data=species.long3, 
 #                aes(x=0, y=0, xend=axis1, yend=axis2), 
#                 colour="red", size=0.2) +
#    geom_text_repel(data=species.long3, 
#                    aes(x=axis1, y=axis2, label=labels),
#                    colour="red",size=2) +
    geom_segment(data=sc_bp1, 
                 aes(x=0, y=0, xend=RDA1, yend=RDA2), 
                 colour="blue", size=0.2,arrow=arrow())+ #+
    geom_text_repel(data=sc_bp1, 
                    aes(x=RDA1, y=RDA2, label=rownames(sc_bp)),
                    colour="blue",size=4) +
  ggtitle("RDA ordination biplot, environmental variables and frequency of NLR loci",subtitle = "across 20 S.chilense populations (scaling 1)")+
  theme_minimal()+
#    BioR.theme +
#    ggsci::scale_colour_npg() +
  scale_fill_manual(values = c("Central Group"="#F8766D","Highland"="#00A5FF","Coast"="#00B81F"))+
    coord_fixed(ratio=1)

plotgg2
getwd()
ggsave("RDA_ALL_sc1.pdf")
```


```{r}
# Custom triplot code!

#extract % explained by the first 2 axes
perc <- round(100*(summary(spe.rda.signif)$cont$importance[2, 1:2]), 2)

#extract scores - these are coordinates in the RDA space
sc_si <- vegan::scores(spe.rda.signif, display="sites", choices=c(1,2), scaling=1)
sc_sp <- vegan::scores(spe.rda.signif, display="species", choices=c(1,2), scaling=1)
sc_bp <- vegan::scores(spe.rda.signif, display="bp", choices=c(1, 2), scaling=1)


```


```{r}
#Here I am going to run a Partial RDA, where I will morel the frequency of certain NLRs with respect to the climatic variables, while taking into account that certain populations are part of the same valley (phylogeographic structure)

#First I am going to do this with the example from R


## Partial RDA: effect of environmental variables, holding population structure
## constant

# Simple syntax; X and W may be in separate tables of quantitative 
# variables

#spe.rda.signif<-rda(spe.hel ~ ., data = env.z)
env.valley<-env.all$Label3
env.valley<-as.factor(env.valley)

spe.env.valley <- rda(spe.hel, env.z, env.valley)
summary(spe.env.valley)


# Test of the partial RDA, using the results with the formula 
# interface to allow the tests of the axes to be run
anova(spe.env.valley, permutations = how(nperm = 999))
#anova(spe.env.valley, permutations = how(nperm = 999), by = "axis")


# Source additional functions that will be used later in this
# Chapter. Our scripts assume that files to be read are in
# the working directory.
source("C://Users/edeli/OneDrive/Projets_Recherche/2021_Tomato_herbarium_Rgenes/2022_DTGenome_TC/S_chilense_annotations/RDA_Analysis/tutorials/triplot.rda.R")

# Partial RDA triplots (with fitted site scores) 
# with function triplot.rda
# Scaling 1

triplot.rda(spe.env.valley, 
#  site.sc = "lc", 
  scaling = 1, 
  cex.char2 = 0.8, 
  pos.env = 3, 
  mar.percent = 0
)
# text(-0.58, 0.64, "a", cex = 2)
# Scaling 2
triplot.rda(spe.env.valley, 
#  site.sc = "lc", 
  scaling = 2, 
  cex.char2 = 0.8, 
  pos.env = 3, 
  arrows.only = FALSE,
mult.spe=0,
  mar.percent = 0.04
)
# text(-3.34, 3.64, "b", cex = 2)

autoplot(spe.env.valley)

```

```{r}
# Plotting it with ggplot2

################ 
# For scaling 1
plot2a<-ordiplot(spe.env.valley,choices=c(1,2), scaling=1)#This is in scaling 1


#This helps calculate the different RDA scores that will be useful for plotting.
sites.long2a <- sites.long(plot2a, env.data=env.z)
head(sites.long2a)
sites.long2a$label2<-c("Southern Coastal Group",rep("Central Group",18), "Southern Highland Group")
sites.long2a$Altitude<-c(346.3,1337,1412.5,2217,2735,1422.5,1748,2632,3073.5,3094.5,351,816,1102,1944.5,3292,1621,1903,2805,3483,2793)

species.long2a <- species.long(plot2a)
species.long2a

axis.long2a <- axis.long(spe.env.valley, choices=c(1, 2))
axis.long2a

sc_bpa <- as.data.frame(vegan::scores(spe.env.valley, display="bp", choices=c(1, 2), scaling=1))



```
```{r}
plotgg2 <- ggplot() + 
    geom_vline(xintercept = c(0), color = "grey70", linetype = 2) +
    geom_hline(yintercept = c(0), color = "grey70", linetype = 2) +  
    xlab(axis.long2a[1, "label"]) +
    ylab(axis.long2a[2, "label"]) +  
    scale_x_continuous(sec.axis = dup_axis(labels=NULL, name=NULL)) +
    scale_y_continuous(sec.axis = dup_axis(labels=NULL, name=NULL)) +    
    geom_point(data=sites.long2a, 
               aes(x=axis1, y=axis2, color=label2, shape=label2), 
               size=3) +
      geom_text_repel(data=sites.long2a, 
                    aes(x=axis1, y=axis2, label=labels),
                    colour="black",size=3) +
#      geom_point(data=species.long2a, 
#               aes(x=axis1, y=axis2),colour="red", 
#               size=1) +
#    geom_segment(data=species.long2a, 
#                 aes(x=0, y=0, xend=axis1, yend=axis2), 
#                 colour="red", size=0.2) +
#    geom_text_repel(data=species.long2a, 
#                    aes(x=axis1, y=axis2, label=labels),
#                    colour="red",size=2) +
    geom_segment(data=sc_bpa, 
                 aes(x=0, y=0, xend=RDA1, yend=RDA2), 
                 colour="blue", size=0.2,arrow=arrow())+ #+
#    geom_text_repel(data=sc_bpa, 
#                    aes(x=RDA1, y=RDA2, label=rownames(sc_bpa)),
#                    colour="blue",size=4) +
  ggtitle("Partial RDA ordination biplot, environmental variables, NLR frequencies",subtitle="holding population structure constant (scaling 1)")+
  theme_minimal()+
#    BioR.theme +
#    ggsci::scale_colour_npg() +
  scale_fill_manual(values = c("Central Group"="#F8766D","Highland"="#00A5FF","Coast"="#00B81F"))+
    coord_fixed(ratio=1)

plotgg2
ggsave("partial_RDA_ALL_sc1.pdf")
```


When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the editor. Consequently, unlike *Knit*, *Preview* does not run any R code chunks. Instead, the output of the chunk when it was last run in the editor is displayed.
