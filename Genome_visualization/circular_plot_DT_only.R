#########################################################
# Script for the visualisation of the Dovetail assembly of Solanum chilense.
#
# By Edeline Gagnon
# Last updated: 20th of June 2023
#
#Scrip was adapted from this tutorial
#https://www.royfrancis.com/beautiful-circos-plots-in-r/


#read in the output.bed file from NBLARC
path.bed<-("C://Users/Phyto1/OneDrive/Projets_Recherche/2021_Tomato_herbarium_Rgenes/2022_DTGenome_TC/circular_plot/Schilense_DT_data/SolChilDove_NLRoutput/output.bed")
bed <- read.table(path.bed,header = FALSE, sep="\t",stringsAsFactors=FALSE, quote="")

head(bed) #V4 has the names of the loci
dim(bed)#291

#read in the annotation file for the entire genome of Solanum chilense
path.bed2<-("C://Users/Phyto1/OneDrive/Projets_Recherche/2021_Tomato_herbarium_Rgenes/2022_DTGenome_TC/circular_plot/Schilense_DT_data/Schilense_Dovetail_Popgen/Schilense_ref_annot.bed")
bed.all <- read.table(path.bed2,header = FALSE, sep="\t",stringsAsFactors=FALSE, quote="")
head(bed.all)
dim(bed.all)#365285     10

#Read the reference file

path.ref<-("C://Users/Phyto1/OneDrive/Projets_Recherche/2021_Tomato_herbarium_Rgenes/2022_DTGenome_TC/circular_plot/Schilense_DT_data/SolChilDove_NLRoutput/Solanum_chilense_v3.2_DOVE_comparison_v4_corrections.csv")
ref <- read.csv(path.ref,header = TRUE, stringsAsFactors=FALSE, quote="")
head(ref)

path.ref2<-("C://Users/Phyto1/OneDrive/Projets_Recherche/2021_Tomato_herbarium_Rgenes/2022_DTGenome_TC/circular_plot/Schilense_DT_data/SolChilDove_NLRoutput/Solanum_chilense_v3.2_DOVE_comparison_v3.csv")
ref2 <- read.csv(path.ref2,header = TRUE, stringsAsFactors=FALSE, quote="")
head(ref2)

ref2[(237:346),] #This corresponds to the part of the table that indicates the sequence name, and where it is in the genome.

############ subset the whole annotation.


#subset the twelve chromosomes
unique(bed.all$V1)
length(unique(bed.all$V1)) #76
table(bed.all$V1)

#Subset the bed file to only retain the 12 biggest scaffolds, which are the chromosomes
chr<-c("Scaffold_12631_Chr1","Scaffold_12628_Chr2","Scaffold_12630_Chr3","Scaffold_12634_Chr4", "Scaffold_12633_Chr5", "Scaffold_8756_Chr6", "Scaffold_12636_Chr7", "Scaffold_12635_Chr8", "Scaffold_12638_Chr9","Scaffold_12637_Chr10","Scaffold_12632_Chr11","Scaffold_12629_Chr12")

bed2.all<-bed.all[bed.all$V1 %in% chr, c(1:4,8,10)]
head(bed2.all)

bed2.all
bed2.all[ ,1] = paste0("DT_", bed2.all[, 1])

library(stringr)
w1<-word(bed2.all$V1,1,sep="_")
w2<-word(bed2.all$V1,4,sep="_")

w3<-paste(w1,w2,sep="_")
bed2.all$V1<-w3

y2<-c("DT_Chr1","DT_Chr2","DT_Chr3","DT_Chr4","DT_Chr5","DT_Chr6","DT_Chr7","DT_Chr8","DT_Chr9","DT_Chr10","DT_Chr11","DT_Chr12")

#x[order(match(x, y))]

bed2.all<-bed2.all[order(match(bed2.all$V1, y2)),]
bed2.all$V1

head(bed2.all)

# Only retain mRNA

bed3.all<-bed2.all[bed2.all$V8 == "mRNA", ]
dim(bed3.all)#38795 mRNA

################################

#subset the twelve chromosomes
unique(bed$V1)
length(unique(bed$V1)) #76
table(bed$V1)

#Subset the bed file to only retain the 12 biggest scaffolds, which are the chromosomes
chr<-c("Scaffold_12631_Chr1","Scaffold_12628_Chr2","Scaffold_12630_Chr3","Scaffold_12634_Chr4", "Scaffold_12633_Chr5", "Scaffold_8756_Chr6", "Scaffold_12636_Chr7", "Scaffold_12635_Chr8", "Scaffold_12638_Chr9","Scaffold_12637_Chr10","Scaffold_12632_Chr11","Scaffold_12629_Chr12")

bed2<-bed[bed$V1 %in% chr, c(1:4,10)]
head(bed2)

bed2
bed2[ ,1] = paste0("DT_", bed2[, 1])

library(stringr)
w1<-word(bed2$V1,1,sep="_")
w2<-word(bed2$V1,4,sep="_")

w3<-paste(w1,w2,sep="_")
bed2$V1<-w3

y2<-c("DT_Chr1","DT_Chr2","DT_Chr3","DT_Chr4","DT_Chr5","DT_Chr6","DT_Chr7","DT_Chr8","DT_Chr9","DT_Chr10","DT_Chr11","DT_Chr12")

#x[order(match(x, y))]

bed2<-bed2[order(match(bed2$V1, y2)),]
bed2$V1

head(bed2)


#difference between bed and bed2 file
dim(bed)#291
dim(bed2)#285
dim(ref)#179

#Bed2 still has more than the 177 genes in the Nbarc files (with Ref).

#Subsetting the bed2 file so that I only retain the 177 genes in ref

bed3<-bed2[bed2$V4 %in% ref$OldName, ]
dim(bed3)#177

head(bed3)
head(ref) 

#Now add the NewName, the Cluster and NewGene columns

bed3$Cluster <- ref$Cluster[match(bed3$V4, ref$OldName)]
dim(bed3)

bed3$NewName <- ref$NewName[match(bed3$V4, ref$OldName)]
dim(bed3)

bed3$NewGene <- ref$NewGene[match(bed3$V4, ref$OldName)]
dim(bed3)


chr3<-unique(bed3$V1)

bed3[, 1] = factor(bed3[ ,1], levels = chr3)
head(bed3)


bed3<-bed3[order(match(bed3$V1, y2)),]
bed3$V1<-as.character(bed3$V1)

head(bed3)



#Now adding a color column
x1<-c("NA","CNL-RPW8",     "CNL20","CNL1/9",     "CNL11","CNL12","CNL13","CNL2",      "CNL22","CNL4","CNL6","CNL8","NRC","CNL21","ind"," ","TNL")
x2<-c("black","cadetblue1","purple","aquamarine","cyan","green","orange","darkgreen","yellow3","hotpink","darkblue","deepskyblue","red","orangered","grey58","black","yellow")
col.tb<-data.frame(x1,x2)

bed3$col <- col.tb$x2[match(bed3$Cluster, col.tb$x1)]

#Now adding a color column
x3<-c("","New")
x4<-c("black","red")
col.tb2<-data.frame(x3,x4)

bed3$col2 <- col.tb2$x4[match(bed3$NewGene, col.tb2$x3)]

dim(bed3)

#Here remove the 7 loci that were deleted (duplicates, overlap)
remove<-c("Schil_NBS_045", "Schil_NBS_064", "Schil_NBS_080", "Schil_NBS_107", "Schil_NBS_117", "Schil_NBS_132", "Schil_NBS_141")

bed3<-bed3[!bed3$NewName %in% remove, ]

#################
# Now plotting the chromosomes with the 

library(circlize)
region.all<-bed3.all[,2:3]
region<-bed3[,2:3]
value<-bed3[,4:10]
track.margin = c(0, 0)
track.height = 0.1
col.nlr<-value[[6]]
col_text <- "grey40"
#I need 15 values for the colors. I will take them from the 

#circos.par(gap.after = c(rep(1, 11), 5, rep(1, 11), 5))
setwd("C://Users/Phyto1/OneDrive/Projets_Recherche/2021_Tomato_herbarium_Rgenes/2022_DTGenome_TC/circular_plot/")
pdf("Circos_DT_onlyNLR_geneDensity_v2.pdf", width=8, height=12)


circos.clear()
circos.par("track.height"=0.1, gap.degree=5, cell.padding=c(0, 0, 0, 0))

circos.genomicInitialize(bed2.all,plotType="labels")
text(0, 0, "S. chilense DT", cex = 1)
#circos.genomicInitialize(bed3)

### Add names of the chromosomes

# adds genome names as an inside track
circos.track(ylim=c(0, 1), panel.fun=function(x, y) {
  chr=CELL_META$sector.index
  xlim=CELL_META$xlim
  ylim=CELL_META$ylim
#  circos.text(mean(xlim), mean(ylim), chr, cex=0.5, col="white", 
#              facing="bending.inside", niceFacing=TRUE)
}, bg.col="grey60", bg.border=F, track.height=0.03)

circos.track(track.index = get.current.track.index(), panel.fun = function(x, y) {
  circos.axis(h="top",labels.cex=0.1,labels.facing="clockwise",col=col_text, labels.col=col_text, lwd=0)
},bg.border=F)


## Adds the NLR, colored according to cluster

circos.genomicDensity(bed3.all[1:3 ], col = c("#FF000080"), track.height = 0.1)

#circos.genomicInitialize(chromInfo)
#circos.genomicTrackPlotRegion(bed3, ylim = c(0, 1), bg.border = NA, track.height = track.height,
#                              panel.fun = function(region, value, ...) {
#                                col = cytoband.col(value[[6]]) #This selects the NLR.
#                                circos.genomicRect(region, value$Cluster, ybottom = 0, ytop = 1, border=value$col)
#                                xlim = get.cell.meta.data("xlim")
#                                circos.rect(xlim[1], 0, xlim[2], 1, border = "black")
#                              }, cell.padding = c(0, 0, 0, 0), track.margin = track.margin, bg.col="grey90"
#)


#circos.genomicTrack(bed3, panel.fun = function(region, value, ...) {
#  circos.genomicPoints(region, value$NewGene, cex = 0.2, track.height=0.1,...)
#})


#circos.genomicInitialize(chromInfo)
circos.genomicTrackPlotRegion(bed3, ylim = c(0, 1), bg.border = NA, track.height = track.height,
                              panel.fun = function(region, value, ...) {
                                #                                col = cytoband.col(value[[6]]) #This selects the NLR.
                                  circos.genomicRect(region, value$NewGene, ybottom = 0, ytop = 1, border=value$col2)
                                xlim = get.cell.meta.data("xlim")
                                circos.rect(xlim[1], 0, xlim[2], 1, border = "black")
                              }, cell.padding = c(0, 0, 0, 0), track.margin = track.margin, bg.col="grey90"
)


circos.genomicLabels(bed3, labels.column=6, cex=0.25, col=value$col, line_lwd=0.5, line_col=value$col, 
                     side="inside", connection_height=0.1, labels_height=0.04)


 
dev.off()

