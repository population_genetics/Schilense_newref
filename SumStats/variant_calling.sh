#!/bin/bash

beddir=/<some_dir>/NLR_annotation
outdir=/<some_dir>/capture/06_map_calling/bcftools
ref=/<some_dir>/S_chilense_reference_rename.fasta

# bcftools mpileup -f $ref -b bam_list_Schil.txt -R $beddir/NBS_regions.bed --annotate "DP" | \
# bcftools call -m -Oz -f GQ | \
# bcftools sort -o $outdir/Schil_capture_variants_NBSonly_sorted.vcf

# create a filtered VCF containing only invariant sites
vcftools --vcf $outdir/Schil_capture_variants_NBSonly_sorted.vcf \
--max-maf 0 \
--remove-indels \
--minQ 30 \
--minDP 5 \
--max-meanDP 200 \
--keep samples_subset.txt \
--recode --stdout | bgzip -c > $outdir/Schil_NBSonly_invariant_filt.vcf.gz

# create a filtered VCF containing only variant sites
vcftools --vcf $outdir/Schil_capture_variants_NBSonly_sorted.vcf \
--mac 1 \
--remove-indels \
--minQ 30 \
--minDP 5 \
--max-meanDP 200 \
--keep samples_subset.txt \
--recode --stdout | bgzip -c > $outdir/Schil_NBSonly_variant_filt.vcf.gz

# index both vcfs using tabix
tabix $outdir/Schil_NBSonly_invariant_filt.vcf.gz
tabix $outdir/Schil_NBSonly_variant_filt.vcf.gz

# combine the two VCFs using bcftools concat
bcftools concat \
--allow-overlaps \
$outdir/Schil_NBSonly_variant_filt.vcf.gz \
$outdir/Schil_NBSonly_invariant_filt.vcf.gz \
-O z -o $outdir/Schil_NBSonly_filtered.vcf.gz

# index the new vcf using tabix
tabix $outdir/Schil_NBSonly_filtered.vcf.gz
