#!/bin/bash

vcfdir=/<some_dir>/capture/06_map_calling/bcftools
beddir=/<some_dir>/NLR_annotation
outdir=/<some_dir>/capture/08_DivStats_NBS

## calculate Pi and Fst
pixy --stats pi fst dxy \
--vcf $vcfdir/Schil_NBSonly_filtered.vcf.gz \
--populations samples_Populations_subset.txt \
--bed_file $beddir/NBS_regions.bed \
--n_cores 32 \
--output_folder $outdir \
--output_prefix NBS_pixy_output

## calculate allele frequencies for each popualtion separately
cut -f2 samples_Populations_subset.txt | uniq > pop_names.txt

while read pop; do 
	grep $pop samples_Populations_subset.txt | \
	cut -f1 > indvs.$pop.txt
done < pop_names.txt

while read pop; do 
	vcftools --gzvcf $vcf_dir/Schil_NBSonly_variant_filt_headersFIX.vcf.gz \
	--freq --stdout --keep indvs.$pop.txt --bed bed_regions/NB-ARC_regions-156.bed --max-alleles 2 > $vcf_dir/freqs_fix.$pop.txt
done < pop_names.txt
