conda activate mosdepth

for bam in *prop-map.bam; do
	mosdepth -b NBS_regions.bed -t 128 \
	$main_dir/05_depth/NLR_depth/${bam//prop-map.bam/} $bam
done
