ref=/<some_dir>/S_chilense_reference.fasta
out_folder=/<some_dir>/map_calling
threads=64

mkdir /localscratch/bams_SC

cd /some_dir>/Capture-seq_data/01_clean_reads

for sample in *_R1.fq.gz; do

	##### mapping and sort by read name 
	bwa mem -t $threads $ref $sample ${sample//R1/R2} | samtools view -@ $threads -b -o /localscratch/bams_SC/${sample//_R1.fq.gz/_mapped.bam}
	samtools sort -@ $threads -n -O bam -o /localscratch/bams_SC/${sample//_R1.fq.gz/_rn-sorted.bam} /localscratch/bams_SC/${sample//_R1.fq.gz/_mapped.bam}
	samtools flagstat /localscratch/bams_SC/${sample//_R1.fq.gz/_rn-sorted.bam} | sed -n '1p;5p;9p' >> $out_folder/mapping.log.txt

	##### fixmates and sort by coordinate
	samtools fixmate -@ $threads -m -O bam /localscratch/bams_SC/${sample//_R1.fq.gz/_rn-sorted.bam} /localscratch/bams_SC/${sample//_R1.fq.gz/_fixm.bam}
	samtools sort -@ $threads -O bam -o /localscratch/bams_SC/${sample//_R1.fq.gz/_sorted.bam} /localscratch/bams_SC/${sample//_R1.fq.gz/_fixm.bam}

	##### add read groups, remove duplicates and index
	samtools addreplacerg -@ $threads -r ID:${sample//_R1.fq.gz/} -r LB:${sample//_R1.fq.gz/} -r SM:${sample//_R1.fq.gz/} \
	-o /localscratch/bams_SC/${sample//_R1.fq.gz/_RG.bam} /localscratch/bams_SC/${sample//_R1.fq.gz/_sorted.bam}
	samtools markdup -@ $threads -r -S /localscratch/bams_SC/${sample//_R1.fq.gz/_RG.bam} $out_folder/${sample//_R1.fq.gz/_dedup.bam}
	samtools index -@ $threads $out_folder/${sample//_R1.fq.gz/_dedup.bam}
	samtools flagstat $out_folder/${sample//_R1.fq.gz/_dedup.bam} | sed -n '1p;5p;9p' >> $out_folder/mapping.log.txt

	#### keep concordantly mapped reads
	samtools view -@ $threads -h -b -f 3 $out_folder/${sample//_R1.fq.gz/_dedup.bam} > $out_folder/${sample//_R1.fq.gz/_prop-map.bam}
	samtools flagstat $out_folder/${sample//_R1.fq.gz/_prop-map.bam} | sed -n '1p;5p;9p' >> $out_folder/mapping.log.txt
	
	#### keep unmapped reads
	samtools view -@ $threads -b -f 4 $out_folder/${sample//_R1.fq.gz/_dedup.bam} > $out_folder/${sample//_R1.fq.gz/_unmap.bam}
	samtools flagstat $out_folder/${sample//_R1.fq.gz/_unmap.bam} | sed -n '1p;5p;9p' >> $out_folder/mapping.log.txt 

	##### remove intermediate BAMs
	rm -rf /localscratch/bams_SC/*.bam
done
rm -rf /localscratch/bams_SC
